import React, { Component } from 'react'
import ReactPlayer from 'react-player'
import { Widget, addResponseMessage, addLinkSnippet, addUserMessage } from 'react-chat-widget';
//import './style/style.scss';
import './App.css';
import 'react-chat-widget/lib/styles.css';
//https://imr3-react.herokuapp.com/
//https://video-react.js.org/components/player/
//https://www.npmjs.com/package/react-chat-widget
import Chat from './Chat'

const URL = "wss://imr3-react.herokuapp.com";

class App extends Component {
  ws = new WebSocket(URL);

  componentDidMount() {
    addResponseMessage("Welcome to this awesome chat!");
  }

  render () {
    return (
      <div className='app'>
        <section className='section'>
          
          <article id="video-wrapper" className='player'>
          <h1>ReactPlayer Demo</h1>
            <ReactPlayer 
              url='https://www.youtube.com/watch?v=RDoZYgZabr0' 
              playing />
          </article>

          <article id='chat'>
            <Chat />
          </article>

        </section>

      </div>
    );
  }
}

export default App;
