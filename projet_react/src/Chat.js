import React, { Component } from 'react'
import ChatInput from './ChatInput'
import ChatMessage from './ChatMessage'

const URL = 'wss://imr3-react.herokuapp.com'

class Chat extends Component {
  state = {
    name: 'Bob',
    messages: [],
  }

  ws = new WebSocket(URL)

  componentDidMount() {
    this.ws.onopen = () => {
        console.log("connected");
        this.setState({
            connected: true
        });
    };

    this.ws.onmessage = evt => {
        const messages = JSON.parse(evt.data);
        messages.map(message => this.addMessage(message));
        console.log(messages);
    };

    this.ws.onclose = () => {
        console.log("disconnected, reconnect.");
        this.setState({
            connected: false,
            ws: new WebSocket(URL)
        });
    };
  }

  addMessage = message =>
    this.setState(state => ({ messages: [message, ...state.messages] }))

  submitMessage = messageString => {
    // on submitting the ChatInput form, send the message, add it to the list and reset the input
    const message = { name: this.state.name, message: messageString }
    this.ws.send(JSON.stringify(message))
    // this.addMessage(message)
  }

  render() {
    return (
      <div id="chat-wrapper">
        <label htmlFor="name">
          Name:&nbsp;
          <input
            type="text"
            id={'name'}
            class="pseudo"
            placeholder={'Enter your name...'}
            value={this.state.name}
            onChange={e => this.setState({ name: e.target.value })}
          />
        </label>
        <ChatInput
          ws={this.ws}
          onSubmitMessage={messageString => this.submitMessage(messageString)}
        />
        {this.state.messages.map((message, index) =>
          <ChatMessage
            key={index}
            message={message.message}
            name={message.name}
          />,
        )}
      </div>
    )
  }
}

export default Chat