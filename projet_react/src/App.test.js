import React from 'react';
import { render, getByTestId } from '@testing-library/react';
import App from './App';

test("renders without crashing", () => {
  const div = document.createElement("div");
  render(<App />, div);
});

test("contains video wrapper", () => {
  const {container} = render(<App/>);
  const wrapper = container.querySelector(`[id="video-wrapper"]`);
  expect(wrapper).toBeInTheDocument();
});

test("contains title", () => {
  const {container} = render(<App/>);
  const para = container.querySelector("h1");
  expect(para).toBeInTheDocument();
});

test("Chat contains pseudo input", () => {
  const {container} = render(<App/>);
  const chatwrapper = container.querySelector(`[id="chat-wrapper"]`);
  const ipseudo = container.querySelector(`[class="pseudo"]`);
  expect(chatwrapper).toContainElement(ipseudo);
});

test("Chatinput contains text input", () => {
  const {container} = render(<App/>);
  const form = container.querySelector(`[id="form"]`);
  const inputMsg = container.querySelector(`[id="newMsg"]`);
  expect(form).toContainElement(inputMsg);
});



// test('shows new msg when submited',() => {
//   const {container} = render(<App/>);
//   const chatwrapper = container.querySelector(`[id="chat-wrapper"]`);
//   const ipseudo = container.querySelector(`[class="pseudo"]`);
//   const form = container.querySelector(`[id="form"]`);
//   const inputMsg = container.querySelector(`[id="newMsg"]`);
//   ipseudo.value = "Billy";
//   inputMsg.value = "Un Message";
//   form.querySelector(`[type="submit"]`);

//   expect(chatwrapper).toContainElement(querySelector("p").)
// });